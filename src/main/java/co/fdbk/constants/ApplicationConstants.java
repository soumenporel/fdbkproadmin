package co.fdbk.constants;

public class ApplicationConstants {
	public static final String SIGNUP_VIEW = "signup";
	public static final String LOGIN_VIEW = "login";
	public static final String DASHBOARD_VIEW = "dashboard";
	public static final String CATEGORY_VIEW = "category-list";
	public static final String ADD_CATEGORY_VIEW = "add-category";
	public static final String EDIT_CATEGORY_VIEW = "edit-category";
	public static final String CATEGORY_DETAILS_VIEW = "category-details";
	public static final String FORGOTPASSWORD_VIEW = "forgot-password";
	public static final String RESET_PASSWORD_VIEW = "changepass";
	public static final String QUESTION_VIEW = "question";
	public static final String ADD_QUESTION_VIEW = "addquestion";
	public static final String EDIT_QUESTION_VIEW = "editquestion";	
	public static final String QUESTION_DETAILS_VIEW = "questiondetails";
	public static final String SETTINGS_VIEW = "settings";
	public static final String CMS_VIEW = "cms";
	public static final String ADD_CMS_VIEW = "addcms";
	public static final String EDIT_CMS_VIEW = "editcms";
	public static final String CMS_DETAILS_VIEW = "cmsdetails";
	public static final String TEMPLATE_VIEW = "template";
	public static final String ADD_TEMPLATE_VIEW = "addtemplate";
	public static final String EDIT_TEMPLATE_VIEW = "edittemplate";
	public static final String TEMPLATE_DETAILS_VIEW = "templatedetails";
	public static final String CHANGE_PASSWORD_VIEW = "changepassword";
	public static final String EDIT_PROFILE_VIEW = "editprofile";
	public static final String TENANT_VIEW = "tenant";
	public static final String ADD_TENANT_VIEW = "addtenant";
	public static final String EDIT_TENANT_VIEW = "edittenant";
	public static final String TENANT_DETAILS_VIEW = "tenantdetails";
}
