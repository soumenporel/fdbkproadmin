package co.fdbk.controller;

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.PrintWriter;
//import java.net.ServerSocket;
//import java.net.Socket;
//import java.net.UnknownHostException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage; 

public class SendEmail {
	Properties emailProperties;
	static Session mailSession;
	static MimeMessage emailMessage;
	public static void main(String[] args) throws MessagingException {
		
		/*String recipient = "recipient@gmail.com"; 
		  
	      // email ID of  Sender. 
	      String sender = "sender@gmail.com"; 
	  
	      // using host as localhost 
	      String host = "127.0.0.1"; */
	  
	      // Getting system properties 
		
		        String to = "recipient@gmail.com";            // sender email
		        String from = "vikram.singh@cbnits.com";       // receiver email
		        String host = "192.168.1.27";                   // mail server host

		       Properties properties = System.getProperties();
		        properties.setProperty("mail.smtp.host", host);

		        Session session = Session.getDefaultInstance(properties); // default session

		        try {
		            MimeMessage message = new MimeMessage(session);        // email message
		            message.setFrom(new InternetAddress(from));                    // setting header fields
		            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
		            message.setSubject("Test Mail from Java Program"); // subject line

		            // actual mail body
		            message.setText("You can send mail from Java program by using mail API, but you need"
		                    + "couple of more JAR files e.g. smtp.jar and activation.jar");

		            // Send message
		            Transport.send(message);
		            System.out.println("Email Sent successfully....");
		        } catch (MessagingException mex) {
		            mex.printStackTrace();
		        }
		    }

	}

	/*static class SimpleServer implements Runnable {
		@Override
		public void run() {
			ServerSocket serverSocket = null;
			while (true) {
				try {
					serverSocket = new ServerSocket(8080);
					Socket clientSocket = serverSocket.accept();
					BufferedReader inputReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
					System.out.println("Client said :"+inputReader.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}finally{

					try {
						serverSocket.close();
					} catch (IOException e) {
						e.printStackTrace();

					}
				}

			}

		}
	}

	static class SimpleClient implements Runnable {
		@Override
		public void run() {
			Socket socket = null;
			try {
				Thread.sleep(3000);
				socket = new Socket("localhost", 8080);
				PrintWriter outWriter = new PrintWriter(socket.getOutputStream(),true);
				outWriter.println("Hello Mr. Server!");
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}finally{
				try {
					System.out.println("socket is close"+socket.getPort());
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

		}

	}

}*/