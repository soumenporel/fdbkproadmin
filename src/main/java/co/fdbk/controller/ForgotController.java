package co.fdbk.controller;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.IOException;
import java.util.Base64;

import javax.servlet.http.HttpSession;
import javax.ws.rs.core.UriBuilderException;
import co.fdbk.constants.ApplicationConstants;
import co.fdbk.model.ChangePassword;
import co.fdbk.model.Response;
import co.fdbk.model.UserLoginDetails;
import co.fdbk.util.RestClient;

@Controller
public class ForgotController {

	private static final Logger LOG = LoggerFactory.getLogger(ForgotController.class);

	@Value("${forget_password_service}")
	String forgetPasswordService;

	@Value("${reset_password_service}")
	String resetPasswordService;

	@RequestMapping(value = "/forgotPassword", method = RequestMethod.GET)
	public String forgotPassword(HttpSession session) {
		// session.removeAttribute("errmsg");
		LOG.info("Entered in forgotPassword");
		return ApplicationConstants.FORGOTPASSWORD_VIEW;
	}

	@RequestMapping(value = "/forgotPasswordPost", method = RequestMethod.POST)
	public String forgotPasswordPst(@ModelAttribute("detail") UserLoginDetails.Details detail,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in forgotPassword");
		Response details = new Response();
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("emailAddress", detail.getEmailAddress());
		try {
			if (jsonObj != null) {
				String forgetPasswordResponse = RestClient.postObject(String.valueOf(jsonObj), forgetPasswordService);
				ObjectMapper mapper = new ObjectMapper();
				details = mapper.readValue(forgetPasswordResponse, new TypeReference<Response>() {
				});
				System.out.println("Response: " + details);
				if (details.getAck() != 1) {
					System.out.println(details.getMessage());
					redirectAttributes.addFlashAttribute("frgt", details.getMessage());
					return "redirect:/forgotPassword";
				}
			}
		} catch (Exception e) {
			LOG.warn("Exception occured in dologin:{}", e.getMessage());
		}
		redirectAttributes.addFlashAttribute("frgt", details.getMessage());
		return "redirect:/forgotPassword";
	}

	@RequestMapping(value = "/resetPassword/{user_id}", method = RequestMethod.GET)
	public String changePassword(@PathVariable String user_id, HttpSession session, Model model)
			throws NullPointerException, IllegalArgumentException, UriBuilderException, IOException, UnirestException {
		LOG.info("Entered in changePassword");
		String s = user_id;
		String[] data = s.split("=", 2);
		try {
			byte[] decodedBytes = Base64.getDecoder().decode(data[1]);
			String decodedUserId = new String(decodedBytes, "UTF-8");
			model.addAttribute("user_id", decodedUserId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApplicationConstants.RESET_PASSWORD_VIEW;
	}

	@RequestMapping(value = "/doresetPassword", method = RequestMethod.POST)
	public String doresetPassword(@ModelAttribute("resetPass") ChangePassword changepass,
			RedirectAttributes redirectAttributes, Model model) {
		LOG.info("Entered in changePassword");
		try {
			if (changepass != null) {
				String changePasswordResponse = RestClient
						.postObject(RestClient.objectToJson(changepass, ChangePassword.class), resetPasswordService);
				System.out.println("changePasswordResponse values: " + changePasswordResponse);
				ObjectMapper mapper = new ObjectMapper();
				Response details = mapper.readValue(changePasswordResponse, new TypeReference<Response>() {
				});
				if (details.getAck() == 0) {
					redirectAttributes.addFlashAttribute("mssg", details.getMessage());
				} else {
					redirectAttributes.addFlashAttribute("mssg", details.getMessage());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/login";
	}
}
