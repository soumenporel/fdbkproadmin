package co.fdbk.controller;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailController {

	Properties emailProperties;
	Session mailSession;
	MimeMessage emailMessage;

	public void setMailServerProperties() {

		String emailPort = "587";

		emailProperties = System.getProperties();
		emailProperties.put("mail.smtp.port", emailPort);
		emailProperties.put("mail.smtp.auth", "true");
		emailProperties.put("mail.smtp.starttls.enable", "true");

	}

	public String createEmail(String _email,String otp) throws AddressException, MessagingException {
		String[] toEmails = { new String(_email) };
		String emailSubject = "Welcome!";
		String emailBody = "Dear, user! Welcome to FDBKPro! Please enter Otp for verification:"+otp;

		mailSession = Session.getDefaultInstance(emailProperties, null);
		emailMessage = new MimeMessage(mailSession);

		for (int i = 0; i < toEmails.length; i++) {
			emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmails[i]));
		}

		emailMessage.setSubject(emailSubject);
		emailMessage.setContent(emailBody, "text/html");
		return emailBody;
	}

	public int sendEmail() throws AddressException, MessagingException {

		String emailHost = "smtp.gmail.com";
		String fromUser = "mail@natitsolved.com";
		String fromUserEmailPassword = "RaspB#2020";

		Transport transport = mailSession.getTransport("smtp");

		transport.connect(emailHost, fromUser, fromUserEmailPassword);
		transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
		transport.close();
		return 1;

	}
	
	public static void main(String[] args) {
		
	}

}