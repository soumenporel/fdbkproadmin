package co.fdbk.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.fdbk.constants.ApplicationConstants;
import co.fdbk.model.Response;
import co.fdbk.model.Settings;
import co.fdbk.util.RestClient;

@Controller
public class SettingsController {

	private static final Logger LOG = LoggerFactory.getLogger(SettingsController.class);

	@Value("${settings_service}")
	String settingService;

	@Value("${edit_settings_service}")
	String editSettingService;

	@RequestMapping(value = "/settings", method = RequestMethod.GET)
	public String settings(HttpSession session, Model model, RedirectAttributes redirectAttributes) {
		LOG.info("Entered in settings");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String settingsResponse = RestClient.callServiceWithGet(settingService);
			System.out.println("settingsResponse values :" + settingsResponse);
			ObjectMapper mapper = new ObjectMapper();
			Settings details = mapper.readValue(settingsResponse, new TypeReference<Settings>() {
			});
			System.out.println("Settings values :{}" + details.toString());
			model.addAttribute("settings", details);
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApplicationConstants.SETTINGS_VIEW;
	}

	@RequestMapping(value = "/editsettings", method = RequestMethod.POST)
	public String editsettings(@ModelAttribute("edit") Settings edit, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in editsettings");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String editsettingsResponse = RestClient.postObject(RestClient.objectToJson(edit, Settings.class),
					editSettingService);
			System.out.println("settingsResponse values :" + editsettingsResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response details = mapper.readValue(editsettingsResponse, new TypeReference<Response>() {
			});
			System.out.println("Settings values :{}" + details.toString());
			model.addAttribute("settings", details);
			System.out.println("RESPONSE: " + details.getMessage() + ", " + details.getAck());
			redirectAttributes.addFlashAttribute("ack", details.getAck());
			redirectAttributes.addFlashAttribute("errmsg", details.getMessage());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/settings";
	}
}
