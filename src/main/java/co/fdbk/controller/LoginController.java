package co.fdbk.controller;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import co.fdbk.constants.ApplicationConstants;
import co.fdbk.model.Category;
import co.fdbk.model.EditQuestion;
import co.fdbk.model.AddQuestion;
import co.fdbk.model.AddQ;
import co.fdbk.model.Login;
import co.fdbk.model.QuestionCategoryWise;
import co.fdbk.model.QuestionDetails;
import co.fdbk.model.QuestionDetailsPayload;
import co.fdbk.model.QuestionPayload;
import co.fdbk.model.Questions;
import co.fdbk.model.Response;
import co.fdbk.model.UserLoginDetails.Details;
import co.fdbk.model.ChangePass;
import co.fdbk.util.RestClient;

@Controller
public class LoginController {

	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);

	@Value("${login_service}")
	String loginService;

	@Value("${user_list_service}")
	String userListService;

	@Value("${category_list_service}")
	String categoryListService;

	@Value("${question_list_service}")
	String questionListService;

	@Value("${add_question_service}")
	String addQuestionService;

	@Value("${edit_question_service}")
	String editQuestionService;

	@Value("${question_details_service}")
	String questionDetailsService;

	@Value("${delete_question_service}")
	String deleteQuestionService;

	@Value("${change_password_service}")
	String changePasswordService;

	@Value("${edit_profile_service}")
	String editProfileService;

	@Value("${user_details_service}")
	String userDetailsService;

	@RequestMapping("/")
	String defaultIndex(Model model, HttpServletRequest request, HttpSession session) {
		LOG.info("Entered defaultIndex method handler.");
		if (session.getAttribute("userDetails") != null) {
			return "redirect:/dashboard";
		} else {
			return "redirect:/login";
		}
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(HttpSession session) {
		LOG.info("Entered in login");
		if (session.getAttribute("userDetails") != null) {
			return "redirect:/dashboard";
		}
		return ApplicationConstants.LOGIN_VIEW;
	}

	@RequestMapping(value = "/dologin", method = RequestMethod.POST)
	public String dologin(@ModelAttribute("user") Login user, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in dologin");
		try {
			if (user != null) {
				String dosignUpResponse = RestClient.postObject(RestClient.objectToJson(user, Login.class),
						loginService);
				JSONObject response = new JSONObject(dosignUpResponse);
				int ack = (int) response.get("ack");
				if (ack == 1) {
					Gson gson = new Gson();
					Details details = gson.fromJson(response.getJSONObject("details").toString(), Details.class);
					LOG.info("User Login details:::{}" + details.toString());
					session.setAttribute("userDetails", details);
					model.addAttribute("userDetails", session.getAttribute("userDetails"));
					LOG.info("getting user details form session:{}" + session.getAttribute("userDetails").toString());
					return "redirect:/dashboard";
				}
			}
		} catch (Exception e) {
			LOG.warn("Exception occured in dologin:{}", e.getMessage());
		}
		redirectAttributes.addFlashAttribute("errmsg", "Email Address or Password does not match");
		return "redirect:/login";
	}

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String dashboard(HttpSession session, Model model, RedirectAttributes redirectAttributes) {
		LOG.info("Entered in dashboard");
		if (session.getAttribute("userDetails") == null) {
			redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		session.getAttribute("userDetails");
		try {
			String userListResponse = RestClient.callServiceWithGet(userListService);
			ObjectMapper mapper = new ObjectMapper();
			List<Details> details = mapper.readValue(userListResponse, new TypeReference<List<Details>>() {
			});
			model.addAttribute("userList", details);
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApplicationConstants.DASHBOARD_VIEW;
	}

	@RequestMapping(value = "/categoryList", method = RequestMethod.GET)
	public String categoryList(HttpSession session, Model model, RedirectAttributes redirectAttributes) {
		LOG.info("Entered in categoryList");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String categoryListResponse = RestClient.callServiceWithGet(categoryListService);
			ObjectMapper mapper = new ObjectMapper();
			List<Category> categoryLists = mapper.readValue(categoryListResponse, new TypeReference<List<Category>>() {
			});
			model.addAttribute("categoryList", categoryLists);
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApplicationConstants.CATEGORY_VIEW;
	}

	@RequestMapping(value = "/questionList/{categoryId}", method = RequestMethod.GET)
	public String questionList(@PathVariable String categoryId, Questions question, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in questionList");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			question.setCategoryId(categoryId);
			String questionListResponse = RestClient.postObject(RestClient.objectToJson(question, Questions.class),
					questionListService);
			ObjectMapper mapper = new ObjectMapper();
			List<QuestionCategoryWise> questionLists = mapper.readValue(questionListResponse,
					new TypeReference<List<QuestionCategoryWise>>() {
			});
			model.addAttribute("questionList", questionLists);
			session.setAttribute("catId", categoryId);
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApplicationConstants.QUESTION_VIEW;
	}

	@RequestMapping(value = "/addquestionget", method = RequestMethod.GET)
	public String addquestionget(HttpSession session, Model model, RedirectAttributes redirectAttributes) {
		LOG.info("Entered in addquestionget");
		if (session.getAttribute("userDetails") == null) {
			redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		session.getAttribute("userDetails");
		model.addAttribute("userDetails", session.getAttribute("userDetails"));
		return ApplicationConstants.ADD_QUESTION_VIEW;
	}

	@RequestMapping(value = "/addquestion", method = RequestMethod.POST)
	public String addquestion(@ModelAttribute("qstn") QuestionPayload qstn, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in questionList");
		String categoryId = (String) session.getAttribute("catId");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			List<String> questionText = qstn.getQuestionText();
			List<String> coachingTips = qstn.getCoachingTip();
			List<AddQ> addQs = new ArrayList<AddQ>();
			for (int i = 0; i < questionText.size(); i++) {
				AddQ addQ = new AddQ();
				addQ.setQuestionText(questionText.get(i));
				System.out.println("coachingTip in for loop" + coachingTips.get(i));
				addQ.setCoachingTip(coachingTips.get(i));
				addQs.add(addQ);
			}
			AddQuestion questn = new AddQuestion();
			questn.setCategoryId((String) session.getAttribute("catId"));
			questn.setQuestions(addQs);
			String addquestionResponse = RestClient.postObject(RestClient.objectToJson(questn, AddQuestion.class),
					addQuestionService);
			System.out.println("dashboard userList values :" + addquestionResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response addquestion = mapper.readValue(addquestionResponse, new TypeReference<Response>() {
			});
			System.out.println("Question list values :{}" + addquestion.toString());
			model.addAttribute("Response", addquestion);
			redirectAttributes.addFlashAttribute("qwerty", "Question(s) added successfully!");
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/questionList/" + categoryId;
	}

	@RequestMapping(value = "/questiondetails/{questionId}", method = RequestMethod.GET)
	public String questiondetails(@ModelAttribute("qstion") QuestionDetailsPayload qstion, HttpSession session,
			Model model, @PathVariable String questionId, RedirectAttributes redirectAttributes) {
		LOG.info("Entered in editquestion");
		session.setAttribute("questId", questionId);
		qstion.setQuestionId(Integer.parseInt(questionId));
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String QuestionDetailsResponse = RestClient
					.postObject(RestClient.objectToJson(qstion, QuestionDetailsPayload.class), questionDetailsService);
			System.out.println("EditQuestion values :" + QuestionDetailsResponse);
			ObjectMapper mapper = new ObjectMapper();
			QuestionDetails QuestionDetails = mapper.readValue(QuestionDetailsResponse,
					new TypeReference<QuestionDetails>() {
			});
			model.addAttribute("Dtls", QuestionDetails);
			session.setAttribute("qstntxt", QuestionDetails.getQuestionText());
			session.setAttribute("ctip", QuestionDetails.getCoachingTip());
			System.out.println("EditQuestion values :{}" + QuestionDetails.toString());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			System.out.println("Some Exception ocurred! " + e);
		}
		return ApplicationConstants.QUESTION_DETAILS_VIEW;
	}

	@RequestMapping(value = "/editquestionget/{questionId}", method = RequestMethod.GET)
	public String editquestionget(@PathVariable String questionId, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in addquestionget");
		if (session.getAttribute("userDetails") == null) {
			redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		System.out.println("question id " + questionId);
		model.addAttribute("questId", questionId);
		session.setAttribute("questId", questionId);
		session.getAttribute("userDetails");
		model.addAttribute("userDetails", session.getAttribute("userDetails"));
		return ApplicationConstants.EDIT_QUESTION_VIEW;
	}

	@RequestMapping(value = "/editquestion", method = RequestMethod.POST)
	public String editquestion(@ModelAttribute("question") EditQuestion question, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in editquestion");
		String questionId = (String) session.getAttribute("questId");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String EditQuestionResponse = RestClient.postObject(RestClient.objectToJson(question, EditQuestion.class),
					editQuestionService);
			System.out.println("EditQuestion values :" + EditQuestionResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response EditQuestion = mapper.readValue(EditQuestionResponse, new TypeReference<Response>() {
			});
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
			System.out.println("EditQuestion values :{}" + EditQuestion.toString());
			model.addAttribute("EditQuestion", EditQuestion);
			redirectAttributes.addFlashAttribute("qwertyu", "Question edited successfully!");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/questiondetails/" + questionId;
	}

	@RequestMapping(value = "/deletequestion", method = RequestMethod.POST)
	public String deletequestion(@ModelAttribute("qstn") QuestionDetailsPayload qstn, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in deletequestion");
		String categoryId = (String) session.getAttribute("catId");
		System.out.println("VALUE: " + categoryId);
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String DeleteQuestionResponse = RestClient
					.postObject(RestClient.objectToJson(qstn, QuestionDetailsPayload.class), deleteQuestionService);
			System.out.println("deletequestionResponse values :" + DeleteQuestionResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response DeleteQuestion = mapper.readValue(DeleteQuestionResponse, new TypeReference<Response>() {
			});
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
			System.out.println("deletequestionResponse values :{}" + DeleteQuestion.toString());
			model.addAttribute("deletequestion: ", DeleteQuestion);
			redirectAttributes.addFlashAttribute("qwerty", "Question deleted successfully!");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/questionList/" + categoryId;
	}

	@RequestMapping(value = "/changepassword", method = RequestMethod.GET)
	public String changepassword(HttpSession session, Model model, RedirectAttributes redirectAttributes) {
		LOG.info("Entered in changepassword");
		if (session.getAttribute("userDetails") == null) {
			redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		Details loginDetails = (Details) session.getAttribute("userDetails");
		String password = loginDetails.getPassword();
		model.addAttribute("Password", password);
		session.getAttribute("userDetails");
		model.addAttribute("userDetails", session.getAttribute("userDetails"));
		return ApplicationConstants.CHANGE_PASSWORD_VIEW;
	}

	@RequestMapping(value = "/dochangepassword", method = RequestMethod.POST)
	public String dochangepassword(@ModelAttribute("pass") ChangePass pass, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) throws NoSuchAlgorithmException, IOException {
		LOG.info("Entered in editquestion");
		if (session.getAttribute("userDetails") == null) {
			redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		Details dtls = (Details) session.getAttribute("userDetails");
		pass.setUserId(dtls.getUserId());
		String ChangePasswordResponse = RestClient.postObject(RestClient.objectToJson(pass, ChangePass.class),
				changePasswordService);
		System.out.println("ChangePasswordResponse values :" + ChangePasswordResponse);
		ObjectMapper mapper = new ObjectMapper();
		Response ChangePassword = mapper.readValue(ChangePasswordResponse, new TypeReference<Response>() {
		});
		System.out.println("ChangePasswordResponse values :{}" + ChangePassword.toString());
		redirectAttributes.addFlashAttribute("ACK", ChangePassword.getAck());
		if (ChangePassword.getAck() == 0) {
			redirectAttributes.addFlashAttribute("errmsg", "Old Password doesn't match!");
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
			return "redirect:/changepassword";
		} else {
			redirectAttributes.addFlashAttribute("errmsg", "Password changed successfully!");
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
			return "redirect:/changepassword";
		}
	}

	@RequestMapping(value = "/editprofile", method = RequestMethod.GET)
	public String editprofile(HttpSession session, Model model, RedirectAttributes redirectAttributes) {
		LOG.info("Entered in editprofile");
		if (session.getAttribute("userDetails") == null) {
			redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		Details edit = new Details();
		Details dtls = (Details) session.getAttribute("userDetails");
		edit.setUserId(dtls.getUserId());
		try {
			String settingsResponse = RestClient.postObject(RestClient.objectToJson(edit, Details.class),
					userDetailsService);
			System.out.println("settingsResponse values :" + settingsResponse);
			ObjectMapper mapper = new ObjectMapper();
			Details details = mapper.readValue(settingsResponse, new TypeReference<Details>() {
			});
			System.out.println("Settings values :{}" + details.toString());
			session.setAttribute("userDetails", details);
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApplicationConstants.EDIT_PROFILE_VIEW;
	}

	@RequestMapping(value = "/doeditprofile", method = RequestMethod.POST)
	public String doeditprofile(@ModelAttribute("edit") Details edit, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) throws IOException {
		LOG.info("Entered in doeditprofile");
		if (session.getAttribute("userDetails") == null) {
			redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		session.removeAttribute("errrmsg");
		Details dtls = (Details) session.getAttribute("userDetails");
		edit.setUserId(dtls.getUserId());
		if (edit.getOriginalFileUrl() == "") {
			edit.setOriginalFileUrl(dtls.getOriginalFileUrl());
		}
		System.out.println("Payload: " + edit.toString());
		try {
			String EditProfileResponse = RestClient.postObject(RestClient.objectToJson(edit, Details.class),
					editProfileService);
			System.out.println("EditProfileResponse values : " + EditProfileResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response EditProfile = mapper.readValue(EditProfileResponse, new TypeReference<Response>() {
			});
			model.addAttribute("Edit", EditProfile);
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
			if (EditProfile.getAck() == 1) {
				redirectAttributes.addFlashAttribute("errmsg", "Profile updated successfully");
			} else {
				redirectAttributes.addFlashAttribute("errmsg", "Profile Not updated");
			}
		} catch (Exception e) {
			System.out.println("Some Exception Occurred! " + e);
		}
		return "redirect:/editprofile";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
		if (session.getAttribute("userDetails") != null) {
			session.removeAttribute("catId");
			session.removeAttribute("userDetails");
			session.removeAttribute("ctip");
			session.removeAttribute("qstntxt");
			session.removeAttribute("questId");
			session.removeAttribute("pageId");
			session.removeAttribute("pageTitle");
			session.removeAttribute("pageContent");
			session.removeAttribute("metaKeyword");
			session.removeAttribute("metaDescription");
			session.removeAttribute("slug");
			session.removeAttribute("isActive");
			session.removeAttribute("emailTitle");
			session.removeAttribute("emailContent");
			session.removeAttribute("name");
			session.removeAttribute("address");
			session.removeAttribute("address2");
			session.removeAttribute("country");
		}
		return "redirect:/login";
	}

}
