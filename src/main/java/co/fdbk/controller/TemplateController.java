package co.fdbk.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.fdbk.constants.ApplicationConstants;
import co.fdbk.model.Email;
import co.fdbk.model.Response;
import co.fdbk.util.RestClient;

@Controller
public class TemplateController {

	private static final Logger LOG = LoggerFactory.getLogger(TemplateController.class);

	@Value("${add_template_service}")
	String addTemplateService;

	@Value("${edit_template_service}")
	String editTemplateService;

	@Value("${delete_template_service}")
	String deleteTemplateService;

	@Value("${templates_list_service}")
	String TemplatesListService;

	@Value("${template_details_service}")
	String TemplateDetailsService;

	@RequestMapping(value = "/template", method = RequestMethod.GET)
	public String template(HttpSession session, Model model, RedirectAttributes redirectAttributes) {
		LOG.info("Entered in template");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String TemplateResponse = RestClient.callServiceWithGet(TemplatesListService);
			System.out.println("TemplateResponse values :" + TemplateResponse);
			ObjectMapper mapper = new ObjectMapper();
			List<Email> details = mapper.readValue(TemplateResponse, new TypeReference<List<Email>>() {
			});
			System.out.println("TemplateResponse values :{}" + details.toString());
			model.addAttribute("Template", details);
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApplicationConstants.TEMPLATE_VIEW;
	}

	@RequestMapping(value = "/addtemplateget", method = RequestMethod.GET)
	public String addtemplateget(HttpSession session, Model model, RedirectAttributes redirectAttributes) {
		LOG.info("Entered in addtemplateget");
		if (session.getAttribute("userDetails") == null) {
			redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		session.getAttribute("userDetails");
		model.addAttribute("userDetails", session.getAttribute("userDetails"));
		return ApplicationConstants.ADD_TEMPLATE_VIEW;
	}

	@RequestMapping(value = "/addtemplate", method = RequestMethod.POST)
	public String addtemplate(@ModelAttribute("temp") Email temp, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in addtemplate");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String AddTemplateResponse = RestClient.postObject(RestClient.objectToJson(temp, Email.class),
					addTemplateService);
			System.out.println("AddTemplateResponse values :" + AddTemplateResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response AddTemplate = mapper.readValue(AddTemplateResponse, new TypeReference<Response>() {
			});

			System.out.println("AddCMSResponse values :{}" + AddTemplate.toString());
			model.addAttribute("AddTemplate", AddTemplate);
			redirectAttributes.addFlashAttribute("errmsg", AddTemplate.getMessage());
			redirectAttributes.addFlashAttribute("ack", AddTemplate.getAck());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/template";
	}

	@RequestMapping(value = "/templatedetails", method = RequestMethod.POST)
	public String templatedetails(@ModelAttribute("tem") Email tem, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in templatedetails");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String templatedetailsResponse = RestClient.postObject(RestClient.objectToJson(tem, Email.class),
					TemplateDetailsService);
			System.out.println("templatedetailsResponse values :" + templatedetailsResponse);
			ObjectMapper mapper = new ObjectMapper();
			Email details = mapper.readValue(templatedetailsResponse, new TypeReference<Email>() {
			});
			System.out.println("templatedetailsResponse values :{}" + details.toString());
			model.addAttribute("templatedetails", details);
			session.setAttribute("emailTitle", details.getEmailTitle());
			session.setAttribute("emailContent", details.getEmailContent());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApplicationConstants.TEMPLATE_DETAILS_VIEW;
	}

	@RequestMapping(value = "/edittemplateget/{emailTemplateId}", method = RequestMethod.GET)
	public String editCMSget(@PathVariable String emailTemplateId, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in edittemplateget");
		if (session.getAttribute("userDetails") == null) {
			redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		System.out.println("emailTemplateId" + emailTemplateId);
		model.addAttribute("emailTemplateId", emailTemplateId);
		session.getAttribute("userDetails");
		model.addAttribute("userDetails", session.getAttribute("userDetails"));
		return ApplicationConstants.EDIT_TEMPLATE_VIEW;
	}

	@RequestMapping(value = "/edittemplate", method = RequestMethod.POST)
	public String editCMS(@ModelAttribute("email") Email email, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in edittemplate");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String EditTemplateResponse = RestClient.postObject(RestClient.objectToJson(email, Email.class),
					editTemplateService);
			System.out.println("EditTemplateResponse values :" + EditTemplateResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response EditTemplate = mapper.readValue(EditTemplateResponse, new TypeReference<Response>() {
			});

			System.out.println("EditTemplateResponse values :{}" + EditTemplate.toString());
			model.addAttribute("EditTemplate", EditTemplate);
			redirectAttributes.addFlashAttribute("errmsg", EditTemplate.getMessage());
			redirectAttributes.addFlashAttribute("ack", EditTemplate.getAck());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/template";
	}

	@RequestMapping(value = "/deletetemplate", method = RequestMethod.POST)
	public String deletetemplate(@ModelAttribute("temd") Email temd, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in deletetemplate");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String DeleteTemplateResponse = RestClient.postObject(RestClient.objectToJson(temd, Email.class),
					deleteTemplateService);
			System.out.println("DeleteTemplateResponse values :" + DeleteTemplateResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response DeleteTemplate = mapper.readValue(DeleteTemplateResponse, new TypeReference<Response>() {
			});

			System.out.println("DeleteTemplateResponse values :{}" + DeleteTemplate.toString());
			model.addAttribute("DeleteTemplate", DeleteTemplate);
			redirectAttributes.addFlashAttribute("errmsg", DeleteTemplate.getMessage());
			redirectAttributes.addFlashAttribute("ack", DeleteTemplate.getAck());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/template";
	}
}
