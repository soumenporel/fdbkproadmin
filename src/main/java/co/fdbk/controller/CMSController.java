package co.fdbk.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.fdbk.constants.ApplicationConstants;
import co.fdbk.model.CMS;
import co.fdbk.model.Response;
import co.fdbk.util.RestClient;

@Controller
public class CMSController {

	private static final Logger LOG = LoggerFactory.getLogger(CMSController.class);

	@Value("${add_content_service}")
	String addContentService;

	@Value("${edit_content_service}")
	String editContentService;

	@Value("${delete_content_service}")
	String deleteContentService;

	@Value("${contents_list_service}")
	String ContentsListService;

	@Value("${content_details_service}")
	String ContentDetailsService;

	@RequestMapping(value = "/CMS", method = RequestMethod.GET)
	public String CMS(HttpSession session, Model model, RedirectAttributes redirectAttributes) {
		LOG.info("Entered in CMS");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String CMSResponse = RestClient.callServiceWithGet(ContentsListService);
			System.out.println("CMSResponse values :" + CMSResponse);
			ObjectMapper mapper = new ObjectMapper();
			List<CMS> details = mapper.readValue(CMSResponse, new TypeReference<List<CMS>>() {
			});
			System.out.println("CMSResponse values :{}" + details.toString());
			model.addAttribute("CMS", details);
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApplicationConstants.CMS_VIEW;
	}

	@RequestMapping(value = "/addCMSget", method = RequestMethod.GET)
	public String addCMSget(HttpSession session, Model model, RedirectAttributes redirectAttributes) {
		LOG.info("Entered in addCMSget");
		if (session.getAttribute("userDetails") == null) {
			redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		session.getAttribute("userDetails");
		model.addAttribute("userDetails", session.getAttribute("userDetails"));
		return ApplicationConstants.ADD_CMS_VIEW;
	}

	@RequestMapping(value = "/addCMS", method = RequestMethod.POST)
	public String addCMS(@ModelAttribute("cms") CMS cms, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in addCMS");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String AddCMSResponse = RestClient.postObject(RestClient.objectToJson(cms, CMS.class), addContentService);
			System.out.println("AddCMSResponse values :" + AddCMSResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response AddCMS = mapper.readValue(AddCMSResponse, new TypeReference<Response>() {
			});

			System.out.println("AddCMSResponse values :{}" + AddCMS.toString());
			model.addAttribute("AddCMS", AddCMS);
			redirectAttributes.addFlashAttribute("cms", AddCMS.getMessage());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/CMS";
	}

	@RequestMapping(value = "/CMSdetails", method = RequestMethod.POST)
	public String CMSdetails(@ModelAttribute("cms") CMS cms, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in CMS");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String CMSdetailsResponse = RestClient.postObject(RestClient.objectToJson(cms, CMS.class),
					ContentDetailsService);
			System.out.println("CMSdetailsResponse values :" + CMSdetailsResponse);
			ObjectMapper mapper = new ObjectMapper();
			CMS details = mapper.readValue(CMSdetailsResponse, new TypeReference<CMS>() {
			});
			System.out.println("CMSdetailsResponse values :{}" + details.toString());
			model.addAttribute("CMSdetails", details);
			session.setAttribute("pageId", details.getPageId());
			session.setAttribute("pageTitle", details.getPageTitle());
			session.setAttribute("pageContent", details.getPageContent());
			session.setAttribute("metaKeyword", details.getMetaKeyword());
			session.setAttribute("metaDescription", details.getMetaDescription());
			session.setAttribute("slug", details.getSlug());
			session.setAttribute("isActive", details.getIsActive());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApplicationConstants.CMS_DETAILS_VIEW;
	}

	@RequestMapping(value = "/editCMSget/{pageId}", method = RequestMethod.GET)
	public String editCMSget(@PathVariable String pageId, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in editCMSget");
		if (session.getAttribute("userDetails") == null) {
			redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		System.out.println("page id " + pageId);
		model.addAttribute("pageId", pageId);
		session.getAttribute("userDetails");
		model.addAttribute("userDetails", session.getAttribute("userDetails"));
		return ApplicationConstants.EDIT_CMS_VIEW;
	}

	@RequestMapping(value = "/editCMS", method = RequestMethod.POST)
	public String editCMS(@ModelAttribute("cms") CMS cms, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in editCMS");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String EditCMSResponse = RestClient.postObject(RestClient.objectToJson(cms, CMS.class), editContentService);
			System.out.println("EditCMSResponse values :" + EditCMSResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response EditCMS = mapper.readValue(EditCMSResponse, new TypeReference<Response>() {
			});

			System.out.println("EditCMSResponse values :{}" + EditCMS.toString());
			model.addAttribute("EditCMS", EditCMS);
			redirectAttributes.addFlashAttribute("cms", EditCMS.getMessage());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/CMS";
	}

	@RequestMapping(value = "/deleteCMS", method = RequestMethod.POST)
	public String deleteCMS(@ModelAttribute("cmsd") CMS cmsd, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in deleteCMS");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String DeleteCMSResponse = RestClient.postObject(RestClient.objectToJson(cmsd, CMS.class),
					deleteContentService);
			System.out.println("DeleteCMSResponse values :" + DeleteCMSResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response DeleteCMS = mapper.readValue(DeleteCMSResponse, new TypeReference<Response>() {
			});

			System.out.println("DeleteCMSResponse values :{}" + DeleteCMS.toString());
			model.addAttribute("DeleteCMS", DeleteCMS);
			redirectAttributes.addFlashAttribute("cms", DeleteCMS.getMessage());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/CMS";
	}
}
