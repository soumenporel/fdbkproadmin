package co.fdbk.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.fdbk.constants.ApplicationConstants;
import co.fdbk.model.Tenant;
import co.fdbk.model.Response;
import co.fdbk.util.RestClient;

@Controller
public class TenantController {

	private static final Logger LOG = LoggerFactory.getLogger(TenantController.class);

	@Value("${add_tenant_service}")
	String addTenantService;

	@Value("${edit_tenant_service}")
	String editTenantService;

	@Value("${delete_tenant_service}")
	String deleteTenantService;

	@Value("${tenants_list_service}")
	String tenantsListService;

	@Value("${tenant_details_service}")
	String tenantDetailsService;

	@RequestMapping(value = "/tenant", method = RequestMethod.GET)
	public String tenant(HttpSession session, Model model) {
		LOG.info("Entered in tenant");
		try {
			if(session.getAttribute("userDetails") == null) {
				session.setAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String tenantResponse = RestClient.callServiceWithGet(tenantsListService);
			System.out.println("tenantResponse values :" + tenantResponse);
			ObjectMapper mapper = new ObjectMapper();
			List<Tenant> details = mapper.readValue(tenantResponse, new TypeReference<List<Tenant>>() {
			});
			System.out.println("tenantResponse values :{}" + details.toString());
			model.addAttribute("tenant", details);
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApplicationConstants.TENANT_VIEW;
	}

	@RequestMapping(value = "/addtenantget", method = RequestMethod.GET)
	public String addtenantget(HttpSession session, Model model) {
		LOG.info("Entered in addtenantget");
		if(session.getAttribute("userDetails") == null) {
			session.setAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		session.getAttribute("userDetails");
		model.addAttribute("userDetails", session.getAttribute("userDetails"));
		return ApplicationConstants.ADD_TENANT_VIEW;
	}

	@RequestMapping(value = "/addtenant", method = RequestMethod.POST)
	public String addtenant(@ModelAttribute("tenant") Tenant tenant, HttpSession session, Model model) {
		LOG.info("Entered in addtenant");
		try {
			if(session.getAttribute("userDetails") == null) {
				session.setAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String AddtenantResponse = RestClient.postObject(RestClient.objectToJson(tenant, Tenant.class),
					addTenantService);
			System.out.println("AddtenantResponse values :" + AddtenantResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response Addtenant = mapper.readValue(AddtenantResponse, new TypeReference<Response>() {
			});

			System.out.println("AddCMSResponse values :{}" + Addtenant.toString());
			model.addAttribute("Addtenant", Addtenant);
			session.setAttribute("tnt", Addtenant.getMessage());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/tenant";
	}

	@RequestMapping(value = "/tenantdetails", method = RequestMethod.POST)
	public String tenantdetails(@ModelAttribute("tenant") Tenant tenant,HttpSession session, Model model) {
		LOG.info("Entered in tenant");
		try {
			if(session.getAttribute("userDetails") == null) {
				session.setAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String tenantdetailsResponse = RestClient.postObject(RestClient.objectToJson(tenant, Tenant.class),
					tenantDetailsService);
			System.out.println("tenantdetailsResponse values :" + tenantdetailsResponse);
			ObjectMapper mapper = new ObjectMapper();
			Tenant details = mapper.readValue(tenantdetailsResponse, new TypeReference<Tenant>() {
			});
			System.out.println("tenantdetailsResponse values :{}" + details.toString());
			model.addAttribute("tenantdetails", details);
			session.setAttribute("name", details.getOrgName());
			session.setAttribute("address", details.getOrgAddress());
			session.setAttribute("address2", details.getOrgAddress2());
			session.setAttribute("country", details.getOrgCountry());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApplicationConstants.TENANT_DETAILS_VIEW;
	}

	@RequestMapping(value = "/edittenantget/{tenantId}", method = RequestMethod.GET)
	public String edittenantget(@PathVariable String tenantId, HttpSession session, Model model) {
		LOG.info("Entered in edittenantget");
		if(session.getAttribute("userDetails") == null) {
			session.setAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		System.out.println("tenant id " + tenantId);
		model.addAttribute("tenantId", tenantId);
		session.getAttribute("userDetails");
		model.addAttribute("userDetails", session.getAttribute("userDetails"));
		return ApplicationConstants.EDIT_TENANT_VIEW;
	}

	@RequestMapping(value = "/edittenant", method = RequestMethod.POST)
	public String edittenant(@ModelAttribute("tenant") Tenant tenant, HttpSession session, Model model) {
		LOG.info("Entered in edittenant");
		try {
			if(session.getAttribute("userDetails") == null) {
				session.setAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String EdittenantResponse = RestClient.postObject(RestClient.objectToJson(tenant, Tenant.class),
					editTenantService);
			System.out.println("EditCMSResponse values :" + EdittenantResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response Edittenant = mapper.readValue(EdittenantResponse, new TypeReference<Response>() {
			});

			System.out.println("EdittenantResponse values :{}" + Edittenant.toString());
			model.addAttribute("Edittenant", Edittenant);
			session.setAttribute("tnt", Edittenant.getMessage());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/tenant";
	}

	@RequestMapping(value = "/deletetenant", method = RequestMethod.POST)
	public String deletetenant(@ModelAttribute("tenant") Tenant tenant, HttpSession session, Model model) {
		LOG.info("Entered in deletetenant");
		try {
			if(session.getAttribute("userDetails") == null) {
				session.setAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String DeletetenantResponse = RestClient.postObject(RestClient.objectToJson(tenant, Tenant.class),
					deleteTenantService);
			System.out.println("DeletetenantResponse values :" + DeletetenantResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response Deletetenant = mapper.readValue(DeletetenantResponse, new TypeReference<Response>() {
			});

			System.out.println("DeleteCMSResponse values :{}" + Deletetenant.toString());
			model.addAttribute("DeleteCMS", Deletetenant);
			session.setAttribute("tnt", Deletetenant.getMessage());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/tenant";
	}
}
