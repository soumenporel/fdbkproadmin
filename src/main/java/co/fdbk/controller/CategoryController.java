package co.fdbk.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.fdbk.constants.ApplicationConstants;
import co.fdbk.model.Category;
import co.fdbk.model.Email;
import co.fdbk.model.Response;
import co.fdbk.util.RestClient;

@Controller
public class CategoryController {

	private static final Logger LOG = LoggerFactory.getLogger(CategoryController.class);

	@Value("${add_category_service}")
	String addCategoryService;

	@Value("${edit_category_service}")
	String editCategoryService;

	@Value("${delete_category_service}")
	String deleteCategoryService;

	@Value("${categories_details_service}")
	String categoryDetailsService;

	@RequestMapping(value = "/addcategoryget", method = RequestMethod.GET)
	public String addtemplateget(HttpSession session, Model model, RedirectAttributes redirectAttributes) {
		LOG.info("Entered in addcategoryget");
		if (session.getAttribute("userDetails") == null) {
			redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		session.getAttribute("userDetails");
		model.addAttribute("userDetails", session.getAttribute("userDetails"));
		return ApplicationConstants.ADD_CATEGORY_VIEW;
	}

	@RequestMapping(value = "/addcategory", method = RequestMethod.POST)
	public String addtemplate(@ModelAttribute("cat") Category cat, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in addcategory");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String AddCategoryResponse = RestClient.postObject(RestClient.objectToJson(cat, Category.class),
					addCategoryService);
			System.out.println("AddCategoryResponse values :" + AddCategoryResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response AddCategory = mapper.readValue(AddCategoryResponse, new TypeReference<Response>() {
			});

			System.out.println("AddCategoryResponse values :{}" + AddCategory.toString());
			model.addAttribute("AddCategory", AddCategory);
			redirectAttributes.addFlashAttribute("errmsg", AddCategory.getMessage());
			redirectAttributes.addFlashAttribute("ack", AddCategory.getAck());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/categoryList";
	}

	@RequestMapping(value = "/categorydetails", method = RequestMethod.POST)
	public String categorydetails(@ModelAttribute("cat") Category cat, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in categorydetails");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String categorydetailsResponse = RestClient.postObject(RestClient.objectToJson(cat, Category.class),
					categoryDetailsService);
			System.out.println("categorydetailsResponse values :" + categorydetailsResponse);
			ObjectMapper mapper = new ObjectMapper();
			Category details = mapper.readValue(categorydetailsResponse, new TypeReference<Category>() {
			});
			System.out.println("categorydetailsResponse values :{}" + details.toString());
			model.addAttribute("categorydetails", details);
			session.getAttribute("userDetails");
			session.setAttribute("categoryId", details.getCategory_id());
			session.setAttribute("parentCategoryId", details.getParent_category_id());
			session.setAttribute("categoryName", details.getCategory_name());
			session.setAttribute("isActive", details.getIs_active());
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApplicationConstants.CATEGORY_DETAILS_VIEW;
	}

	@RequestMapping(value = "/deletecategory", method = RequestMethod.POST)
	public String deletecategory(@ModelAttribute("cat") Category cat, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in deletecategory");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String DeleteCategoryResponse = RestClient.postObject(RestClient.objectToJson(cat, Category.class),
					deleteCategoryService);
			System.out.println("DeleteCategoryResponse values :" + DeleteCategoryResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response DeleteCategory = mapper.readValue(DeleteCategoryResponse, new TypeReference<Response>() {
			});

			System.out.println("DeleteCategoryResponse values :{}" + DeleteCategory.toString());
			model.addAttribute("DeleteCategory", DeleteCategory);
			redirectAttributes.addFlashAttribute("errmsg", DeleteCategory.getMessage());
			redirectAttributes.addFlashAttribute("ack", DeleteCategory.getAck());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/categoryList";
	}

	@RequestMapping(value = "/editcategoryget/{category_id}", method = RequestMethod.GET)
	public String editcategoryget(@PathVariable String category_id, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in editcategoryget");
		if (session.getAttribute("userDetails") == null) {
			redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
			return "redirect:/login";
		}
		System.out.println("category_id: " + category_id);
		model.addAttribute("category_id", category_id);
		session.getAttribute("userDetails");
		model.addAttribute("userDetails", session.getAttribute("userDetails"));
		return ApplicationConstants.EDIT_CATEGORY_VIEW;
	}

	@RequestMapping(value = "/editcategory", method = RequestMethod.POST)
	public String editcategory(@ModelAttribute("cat") Category cat, HttpSession session, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Entered in editcategory");
		try {
			if (session.getAttribute("userDetails") == null) {
				redirectAttributes.addFlashAttribute("tym", "Session Expired! Please Login again!");
				return "redirect:/login";
			}
			String EditCategoryResponse = RestClient.postObject(RestClient.objectToJson(cat, Category.class),
					editCategoryService);
			System.out.println("EditCategoryResponse values :" + EditCategoryResponse);
			ObjectMapper mapper = new ObjectMapper();
			Response EditCategory = mapper.readValue(EditCategoryResponse, new TypeReference<Response>() {
			});

			System.out.println("EditCategoryResponse values :{}" + EditCategory.toString());
			model.addAttribute("EditCategory", EditCategory);
			redirectAttributes.addFlashAttribute("errmsg", EditCategory.getMessage());
			redirectAttributes.addFlashAttribute("ack", EditCategory.getAck());
			session.getAttribute("userDetails");
			model.addAttribute("userDetails", session.getAttribute("userDetails"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/categoryList";
	}
}
