package co.fdbk.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriBuilderException;

//import org.codehaus.jettison.json.JSONException;
//import org.codehaus.jettison.json.JSONObject;
import org.glassfish.jersey.client.ClientProperties;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

public final class RestClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(RestClient.class);
	private static Client client;
	private static MobiObjectMapper mapper = new MobiObjectMapper();
	private static Map<String, WebTarget> resources = new HashMap<String, WebTarget>();
	private static MobiObjectMapper mobiObjectMapper = new MobiObjectMapper();

	static {
		client = ClientBuilder.newClient();
	}

	private RestClient() {
	}

	public static <T> String objectToJson(Object object, Class<?> clazz) {
		String jsonRequest = null;
		try {
			jsonRequest = mapper.getContext(clazz).writeValueAsString(object);
		} catch (JsonGenerationException e) {
			LOGGER.error("ERROR:: RestClient:: objectToJSON method", e);
		} catch (JsonMappingException e) {
			LOGGER.error("ERROR:: RestClient:: objectToJSON method", e);
		} catch (IOException e) {
			LOGGER.error("ERROR:: RestClient:: objectToJSON method", e);
		}
		return jsonRequest;
	}

	public static WebTarget constructServiceURI(String restServiceURI) throws IllegalArgumentException,
	UriBuilderException {
		WebTarget resource = resources.get(restServiceURI);
		if (resource == null) {
			resource = client.target(UriBuilder.fromUri(restServiceURI).build());
			resources.put(restServiceURI, resource);
		}
		return resource;
	}

	public static Response callServiceWithGet(String restServiceURI, int timeOut, String... requestParams){
		client.property(ClientProperties.READ_TIMEOUT, timeOut);
		client.property(ClientProperties.CONNECT_TIMEOUT, timeOut);
		Response response = callServiceWithGet(restServiceURI,requestParams);
		client.property(ClientProperties.READ_TIMEOUT, null);
		client.property(ClientProperties.CONNECT_TIMEOUT, null);
		return response;
	}

	public static Response callServiceWithGet(String restServiceURI, String... requestParams)
			throws IllegalArgumentException, UriBuilderException {
		LOGGER.debug("Calling service with Get with Request Parameter : {},Service URI {}", new Object[]{requestParams,
				restServiceURI});
		WebTarget service = constructServiceURI(restServiceURI);
		for (String requestParam : requestParams) {
			service = service.path(requestParam);
		}
		Response response = service.request().get();
		LOGGER.debug("response code : {}.", response.getStatus());
		return Response.status(response.getStatus()).entity(response.readEntity(String.class))
				.type(response.getMediaType()).build();
	}

	public static String callServiceWithPost(String jsonRequest, String restServiceURI, int timeOut)
			throws IllegalArgumentException, UriBuilderException, IOException {
		LOGGER.debug("read time out is set to {} ms.", timeOut);
		client.property(ClientProperties.READ_TIMEOUT, timeOut);
		LOGGER.debug("connection time out is set to {} ms.", timeOut);
		client.property(ClientProperties.CONNECT_TIMEOUT, timeOut);
		String response = callServiceWithPost(jsonRequest, restServiceURI);
		client.property(ClientProperties.READ_TIMEOUT, null);
		client.property(ClientProperties.CONNECT_TIMEOUT, null);
		return response;
	}

	public static String callServiceWithPost(String jsonRequest, String restServiceURI)
			throws IllegalArgumentException, UriBuilderException, IOException {
		OkHttpClient client = new OkHttpClient();
		client.setConnectTimeout(900000, TimeUnit.MILLISECONDS);
		
		System.out.println("---------------------------");
		System.out.println("Request => " + jsonRequest);
		System.out.println("---------------------------");

		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType,jsonRequest);
		Request request = new Request.Builder()
				.url(restServiceURI)
				.post(body)
				.addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "2de45479-3df6-8f6d-797a-9e347d3311a3")
				.addHeader("content-type", "application/json")
				.build();

		com.squareup.okhttp.Response response = client.newCall(request).execute();
		
		System.out.println("--------------------------------response-------"+response);
		Headers headers = response.headers();
		System.out.println("myfeedbackResponse values header first call :" +headers.toString());
		System.out.println("---------------------------------------");
		
		System.out.println("Response =>" + response.body().string());
		System.out.println("---------------------------------------");
		int resCode = response.code();
		System.out.println(" resCode values:::::"+resCode);
		if(resCode == 307){
			System.out.println("checking the 307 status");
			OkHttpClient client1 = new OkHttpClient();
			client.setConnectTimeout(150000, TimeUnit.MILLISECONDS);
	
			Request requestRedirect = new Request.Builder()
					.url(headers.get("Location"))
					.post(body)
					.addHeader("content-type", "application/json")
					.build();
			
			return client1.newCall(requestRedirect).execute().body().string();
		}
		return client.newCall(request).execute().body().string();

	}

	public static Response post(String serviceUri, MultivaluedMap<String, Object> headers, String payload)
			throws IllegalArgumentException, UriBuilderException {
		LOGGER.debug("Calling service with Post for the Service URI {} headers:{}", new Object[]{serviceUri, headers});
		Response response = constructServiceURI(serviceUri)
				.request()
				.headers(headers)
				.post(Entity.entity(payload, ""), Response.class);
		LOGGER.debug("response code : {}.", response.getStatus());
		return response;
	}

	public static Object jsonToObject(final String jsonData, Class<?> c) {
		ObjectMapper objectMapper = mobiObjectMapper.getContext(c);
		try {
			return objectMapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true).readValue(jsonData, c);
		} catch (JsonParseException e) {
			LOGGER.error("ERROR:: RestClient:: jsonToObject method", e);
		} catch (JsonMappingException e) {
			LOGGER.error("ERROR:: RestClient:: jsonToObject method", e);
		} catch (IOException e) {
			LOGGER.error("ERROR:: RestClient:: jsonToObject method", e);
		}
		return null;
	}

	public static Response callServiceWithPostRequest(String request,
			String restServiceURI) throws IllegalArgumentException,
	UriBuilderException {
		LOGGER.debug("Calling service with POST with Service URI {}",
				new Object[] { restServiceURI });
		System.out.println("jsonResponse " + restServiceURI);
		WebTarget service = constructServiceURI(restServiceURI);
		/*Response response = service.request(MediaType.APPLICATION_JSON_TYPE)
				.post(Entity.entity(request, MediaType.TEXT_HTML),
						Response.class);*/
		Response response = service.request().post(Entity.json(request),Response.class);

		LOGGER.debug("response code : {}.", response.getStatus());
		return Response.status(response.getStatus())
				.entity(response.readEntity(String.class))
				.type(response.getMediaType()).build();
	}

	public static Response callServiceWithPost(String jsonRequest,
			String restServiceURI, String headerParamName, String headerParamValue , String... requestParams)
					throws IllegalArgumentException, UriBuilderException {
		LOGGER.debug("Calling service with Post for the Service URI {}",
				new Object[] { restServiceURI });

		WebTarget service = constructServiceURI(restServiceURI);
		for (String requestParam : requestParams) {
			service = service.path(requestParam);
		}
		Response response = service
				.request()
				.header( headerParamName, headerParamValue)
				.post(Entity.entity(jsonRequest,""),
						Response.class);

		LOGGER.debug("response code : {}.", service.toString());
		return Response.status(response.getStatus())
				.entity(response.readEntity(String.class))
				.type(response.getMediaType()).build();
	}

	public static Response callServiceWithDelete(String restServiceURI,
			String... requestParams) throws IllegalArgumentException,
	UriBuilderException {
		LOGGER.debug("Calling service with Delete for the Service URI {}",
				new Object[] { restServiceURI });
		WebTarget service = constructServiceURI(restServiceURI);
		for (String requestParam : requestParams) {
			service = service.path(requestParam);
		}
		Response response = service.request()
				.delete();

		LOGGER.debug("response code : {}.", response.getStatus());
		return Response.status(response.getStatus())
				.entity(response.readEntity(String.class))
				.type(response.getMediaType()).build();
	}

	public static Response callServiceWithPostForList(String jsonRequest, String restServiceURI)
			throws IllegalArgumentException, UriBuilderException {
		System.out.println("service url call "+restServiceURI);
		System.out.println("json data "+jsonRequest);
		LOGGER.debug("Calling service with Post for the Service URI when request is JSON array  {}", new Object[]{restServiceURI});
		Response response = constructServiceURI(restServiceURI).request().post(
				Entity.entity(jsonRequest, ""), Response.class);

		LOGGER.debug("response code : {}.", response.getStatus());
		return Response.status(response.getStatus()).entity(response.readEntity(String.class))
				.type(response.getMediaType()).build();
	}

	public static Response callServiceWithMediaTypeGet(String restServiceURI,String mediaType, String... requestParams)
			throws IllegalArgumentException, UriBuilderException {
		LOGGER.debug("Calling service with Get with Request Parameter and Media type : {},Service URI {}", new Object[]{requestParams,
				restServiceURI});
		WebTarget service = constructServiceURI(restServiceURI);
		for (String requestParam : requestParams) {
			service = service.path(requestParam);
		}
		Response response = service.request(mediaType).get();
		LOGGER.debug("response code : {}.", response.getStatus());
		return Response.status(response.getStatus()).entity(response.readEntity(String.class))
				.type(response.getMediaType()).build();
	}

	public static Response callServiceWithPost(String jsonRequest,
			String restServiceURI, String mediaType)
					throws IllegalArgumentException, UriBuilderException {
		System.out.println("service url call "+restServiceURI);
		System.out.println("json data "+jsonRequest);
		LOGGER.debug("Calling service with Post with media type for the Service URI {}",
				new Object[] { restServiceURI });
		WebTarget service = constructServiceURI(restServiceURI);
		Response response = service.request(mediaType)
				.post(Entity.entity(jsonRequest,""),
						Response.class);

		LOGGER.debug("response code : {}.", service.toString());
		return Response.status(response.getStatus())
				.entity(response.readEntity(String.class))
				.type(response.getMediaType()).build();
	}

	public static Response callServiceWithPost(String serviceUri, MultivaluedMap<String, Object> headers, String payload)
			throws IllegalArgumentException, UriBuilderException {
		LOGGER.info("Calling service with Post for the Service URI {} headers:{}", new Object[]{serviceUri, headers});
		Response response = constructServiceURI(serviceUri)
				.request()
				.headers(headers)
				.post(Entity.entity(payload,""), Response.class);
		LOGGER.debug("response code : {}.", response.getStatus());
		return Response.status(response.getStatus())
				.entity(response.readEntity(String.class))
				.type(response.getMediaType()).build();
	}
	
	public static String postObject(String inputJson,String url) throws IOException {
		StringBuilder response = new StringBuilder();
		try {
			String methodType="POST";
			 System.out.println("url: " + url);
			    System.out.println("METHOD : " + methodType);
			    System.out.println("basic auth=");
			    URL url1 = new URL(url);
			    
			    URLConnection e = url1.openConnection();
			    System.out.println("connection created");
			    HttpURLConnection conn = (HttpURLConnection) e;
			    conn.setRequestMethod(methodType);
			    conn.setDoOutput(true);
			      conn.setRequestProperty("Content-Type", "application/json");
			      JSONObject jsonObject=new JSONObject(inputJson);
			      System.out.println("inputJson : " + jsonObject);
			      OutputStream os = conn.getOutputStream();
			      os.write(inputJson.getBytes());
			      os.flush();
			      os.flush();
			    
			    //System.out.println("respose="+in.toString());

			   
					BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					String tempOuput = null;
					while ((tempOuput = br.readLine()) != null) {
						response.append(tempOuput);
					}
					System.out.println("respose="+response.toString());
					return response.toString();
					
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response.toString();

	}
	
	public static String callServiceWithGet(String restServiceURI)
			throws IllegalArgumentException, UriBuilderException, IOException {
		OkHttpClient client = new OkHttpClient();
		client.setConnectTimeout(900000, TimeUnit.MILLISECONDS);
		
		System.out.println("---------------------------");
		
		Request request = new Request.Builder()
				.url(restServiceURI)
				.get()
				.addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "2de45479-3df6-8f6d-797a-9e347d3311a3")
				.addHeader("content-type", "application/json")
				.build();

		com.squareup.okhttp.Response response = client.newCall(request).execute();
		
		System.out.println("--------------------------------response-------"+response);
		Headers headers = response.headers();
		System.out.println("myfeedbackResponse values header first call :" +headers.toString());
		System.out.println("---------------------------------------");
		
		System.out.println("Response =>" + response.body().string());
		System.out.println("---------------------------------------");
		int resCode = response.code();
		System.out.println(" resCode values:::::"+resCode);
		if(resCode == 307){
			System.out.println("checking the 307 status");
			OkHttpClient client1 = new OkHttpClient();
			client.setConnectTimeout(150000, TimeUnit.MILLISECONDS);
	
			Request requestRedirect = new Request.Builder()
					.url(headers.get("Location"))
					.get()
					.addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "2de45479-3df6-8f6d-797a-9e347d3311a3")
					.addHeader("content-type", "application/json")
					.build();
			
			return client1.newCall(requestRedirect).execute().body().string();
		}
		return client.newCall(request).execute().body().string();

	}
	
	public static String postObjectForgetPassword(String inputJson,String url) throws IOException {
		StringBuilder response = new StringBuilder();
		try {
			String methodType="POST";
			 System.out.println("url: " + url);
			    System.out.println("METHOD : " + methodType);
			    System.out.println("basic auth=");
			    URL url1 = new URL(url);
			    
			    URLConnection e = url1.openConnection();
			    System.out.println("connection created");
			    HttpURLConnection conn = (HttpURLConnection) e;
			    conn.setRequestMethod(methodType);
			    conn.setDoOutput(true);
			      conn.setRequestProperty("Content-Type", "application/json");
			      JSONObject jsonObject=new JSONObject(inputJson);
			      System.out.println("inputJson : " + jsonObject);
			      OutputStream os = conn.getOutputStream();
			      os.write(inputJson.getBytes());
			      os.flush();
			      os.flush();
			    
			    //System.out.println("respose="+in.toString());

			   
					BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					String tempOuput = null;
					while ((tempOuput = br.readLine()) != null) {
						response.append(tempOuput);
					}
					System.out.println("respose="+response.toString());
					return response.toString();
					
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response.toString();

	}
	
	
}