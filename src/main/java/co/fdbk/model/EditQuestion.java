package co.fdbk.model;

public class EditQuestion {
	private String questionId;
	private String questionText;
	private String coachingTip;

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public String getCoachingTip() {
		return coachingTip;
	}

	public void setCoachingTip(String coachingTip) {
		this.coachingTip = coachingTip;
	}

	@Override
	public String toString() {
		return "EditQuestion [questionId=" + questionId + ", questionText=" + questionText + ", coachingTip="
				+ coachingTip + "]";
	}
}
