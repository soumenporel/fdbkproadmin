package co.fdbk.model;

import java.util.List;

public class ParentcategoryModel {
	
	private String parentcategory;
	private List<String> childList;
	
	public String getParentcategoryList() {
		return parentcategory;
	}
	public void setParentcategory(String parentcategory) {
		this.parentcategory = parentcategory;
	}
	public List<String> getChildList() {
		return childList;
	}
	public void setChildList(List<String> childList) {
		this.childList = childList;
	}
	@Override
	public String toString() {
		return "ParentcategoryModel [parentcategory=" + parentcategory + ", childList=" + childList + "]";
	}
	
}
