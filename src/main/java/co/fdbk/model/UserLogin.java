package co.fdbk.model;

import java.util.List;

public class UserLogin extends Response {
	private String email;
	private String password;
	public List<Mentorlist> mentorlist;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Mentorlist> getMentorlist() {
		return mentorlist;
	}

	public void setMentorlist(List<Mentorlist> mentorlist) {
		this.mentorlist = mentorlist;
	}

	public static class Mentorlist {
		private String userId;
		private String firstName;
		private String lastName;
		private String userName;
		private String emailAddress;
		private String countryId;
		private String phone;
		private String linkedinId;
		private String linkedinEmail;
		private String linkedinUrl;
		private String jobTitle;
		private String location;
		private String deviceId;
		private String deviceType;
		private String userPass;

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getEmailAddress() {
			return emailAddress;
		}

		public void setEmailAddress(String emailAddress) {
			this.emailAddress = emailAddress;
		}

		public String getCountryId() {
			return countryId;
		}

		public void setCountryId(String countryId) {
			this.countryId = countryId;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getLinkedinId() {
			return linkedinId;
		}

		public void setLinkedinId(String linkedinId) {
			this.linkedinId = linkedinId;
		}

		public String getLinkedinEmail() {
			return linkedinEmail;
		}

		public void setLinkedinEmail(String linkedinEmail) {
			this.linkedinEmail = linkedinEmail;
		}

		public String getLinkedinUrl() {
			return linkedinUrl;
		}

		public void setLinkedinUrl(String linkedinUrl) {
			this.linkedinUrl = linkedinUrl;
		}

		public String getJobTitle() {
			return jobTitle;
		}

		public void setJobTitle(String jobTitle) {
			this.jobTitle = jobTitle;
		}

		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}

		public String getDeviceId() {
			return deviceId;
		}

		public void setDeviceId(String deviceId) {
			this.deviceId = deviceId;
		}

		public String getDeviceType() {
			return deviceType;
		}

		public void setDeviceType(String deviceType) {
			this.deviceType = deviceType;
		}

		public String getUserPass() {
			return userPass;
		}

		public void setUserPass(String userPass) {
			this.userPass = userPass;
		}

		@Override
		public String toString() {
			return "Mentorlist [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName
					+ ", userName=" + userName + ", emailAddress=" + emailAddress + ", countryId=" + countryId
					+ ", phone=" + phone + ", linkedinId=" + linkedinId + ", linkedinEmail=" + linkedinEmail
					+ ", linkedinUrl=" + linkedinUrl + ", jobTitle=" + jobTitle + ", location=" + location
					+ ", deviceId=" + deviceId + ", deviceType=" + deviceType + ", userPass=" + userPass + "]";
		}
		
		
	}

	@Override
	public String toString() {
		return "UserLogin [email=" + email + ", password=" + password + "]";
	}

}
