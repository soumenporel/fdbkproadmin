package co.fdbk.model;

import java.util.List;

public class AddQuestion {
	private String categoryId;
	private List<AddQ> questions;
	/*private List<String> questionText;
	private List<String> coachingTip;*/
	
	public List<AddQ> getQuestions() {
		return questions;
	}
	public void setQuestions(List<AddQ> questions) {
		this.questions = questions;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	@Override
	public String toString() {
		return "AddQuestion [categoryId=" + categoryId + ", questions=" + questions + "]";
	}
	
}