package co.fdbk.model;

import java.util.Date;

public class Settings extends Response {
	private String settingId;
	private String siteEmail;
	private String siteContact;
	private String siteAddress;
	private String feedbackResendDays;
	private String sitePhone;
	private String siteTag;
	private String siteLogo;
	private String copyrightText;
	private String siteFavicon;
	private String metaTitle;
	private String metaDescription;
	private String metaKeyword;
	private Date createDate;

	public String getSettingId() {
		return settingId;
	}

	public void setSettingId(String settingId) {
		this.settingId = settingId;
	}

	public String getSiteEmail() {
		return siteEmail;
	}

	public void setSiteEmail(String siteEmail) {
		this.siteEmail = siteEmail;
	}

	public String getSiteContact() {
		return siteContact;
	}

	public void setSiteContact(String siteContact) {
		this.siteContact = siteContact;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getFeedbackResendDays() {
		return feedbackResendDays;
	}

	public void setFeedbackResendDays(String feedbackResendDays) {
		this.feedbackResendDays = feedbackResendDays;
	}

	public String getSitePhone() {
		return sitePhone;
	}

	public void setSitePhone(String sitePhone) {
		this.sitePhone = sitePhone;
	}

	public String getSiteTag() {
		return siteTag;
	}

	public void setSiteTag(String siteTag) {
		this.siteTag = siteTag;
	}

	public String getSiteLogo() {
		return siteLogo;
	}

	public void setSiteLogo(String siteLogo) {
		this.siteLogo = siteLogo;
	}

	public String getCopyrightText() {
		return copyrightText;
	}

	public void setCopyrightText(String copyrightText) {
		this.copyrightText = copyrightText;
	}

	public String getSiteFavicon() {
		return siteFavicon;
	}

	public void setSiteFavicon(String siteFavicon) {
		this.siteFavicon = siteFavicon;
	}

	public String getMetaTitle() {
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public String getMetaKeyword() {
		return metaKeyword;
	}

	public void setMetaKeyword(String metaKeyword) {
		this.metaKeyword = metaKeyword;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "Settings [settingId=" + settingId + ", siteEmail=" + siteEmail + ", siteContact=" + siteContact
				+ ", siteAddress=" + siteAddress + ", feedbackResendDays=" + feedbackResendDays + ", sitePhone="
				+ sitePhone + ", siteTag=" + siteTag + ", siteLogo=" + siteLogo + ", copyrightText=" + copyrightText
				+ ", siteFavicon=" + siteFavicon + ", metaTitle=" + metaTitle + ", metaDescription=" + metaDescription
				+ ", metaKeyword=" + metaKeyword + ", createDate=" + createDate + "]";
	}

}