package co.fdbk.model;

public class ModelQuestion {
	private String questionId;
	private String isActive;
	private String coachingTip;
	private String questionText;
	private String categoryId;
	
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getCoachingTip() {
		return coachingTip;
	}
	public void setCoachingTip(String coachingTip) {
		this.coachingTip = coachingTip;
	}
	public String getQuestionText() {
		return questionText;
	}
	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	@Override
	public String toString() {
		return "ModelQuestion [questionId=" + questionId + ", isActive=" + isActive + ", coachingTip=" + coachingTip
				+ ", questionText=" + questionText + ", categoryId=" + categoryId + "]";
	}
	
}
