package co.fdbk.model;

public class MyFeedback {
	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "MyFeedback [userId=" + userId + "]";
	}

}
