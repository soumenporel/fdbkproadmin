package co.fdbk.model;

public class Mentors{
	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "Mentors [userId=" + userId + "]";
	}
}