package co.fdbk.model;

public class Feedback {
	private String deadline;
	private String userId;
	private String audio;
	private String status;

	public String getDeadline() {
		return deadline;
	}

	public void setDeadline(String deadline) {
		this.deadline = deadline;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAudio() {
		return audio;
	}

	public void setAudio(String audio) {
		this.audio = audio;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Feedback [deadline=" + deadline + ", userId=" + userId + ", audio=" + audio + ", status=" + status
				+ "]";
	}

}
