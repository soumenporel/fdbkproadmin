package co.fdbk.model;

import java.util.Date;

public class Email extends Response {
	private String emailTemplateId;
	private String emailTitle;
	private String emailContent;
	private Date createDate;
	private String isActive;
	private String isDeleted;

	public String getEmailTemplateId() {
		return emailTemplateId;
	}

	public void setEmailTemplateId(String emailTemplateId) {
		this.emailTemplateId = emailTemplateId;
	}

	public String getEmailTitle() {
		return emailTitle;
	}

	public void setEmailTitle(String emailTitle) {
		this.emailTitle = emailTitle;
	}

	public String getEmailContent() {
		return emailContent;
	}

	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return "Email [emailTemplateId=" + emailTemplateId + ", emailTitle=" + emailTitle + ", emailContent="
				+ emailContent + ", createDate=" + createDate + ", isActive=" + isActive + ", isDeleted=" + isDeleted
				+ "]";
	}
}
