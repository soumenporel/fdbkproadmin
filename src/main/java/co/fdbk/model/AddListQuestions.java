package co.fdbk.model;

import java.util.List;

public class AddListQuestions {
	private List<AddQ> addQuestion;

	public List<AddQ> getAddQuestion() {
		return addQuestion;
	}

	public void setAddQuestion(List<AddQ> addQuestion) {
		this.addQuestion = addQuestion;
	}

	@Override
	public String toString() {
		return "AddListQuestions [addQuestion=" + addQuestion + "]";
	}
}
