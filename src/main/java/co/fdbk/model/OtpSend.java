package co.fdbk.model;

public class OtpSend extends Response {
	private String linkedInId;

	public String getLinkedInId() {
		return linkedInId;
	}

	public void setLinkedInId(String linkedInId) {
		this.linkedInId = linkedInId;
	}

	@Override
	public String toString() {
		return "OtpSend [linkedInId=" + linkedInId + "]";
	}
}