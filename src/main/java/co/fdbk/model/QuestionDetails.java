package co.fdbk.model;

import java.util.Date;

public class QuestionDetails extends Response {
	private int questionId;
	private int categoryId;
	private String questionText;
	private String coachingTip;
	private int isActive;
	private int tenantId;
	private Date createDate;

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public String getCoachingTip() {
		return coachingTip;
	}

	public void setCoachingTip(String coachingTip) {
		this.coachingTip = coachingTip;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public int getTenantId() {
		return tenantId;
	}

	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "QuestionDetails [questionId=" + questionId + ", categoryId=" + categoryId + ", questionText="
				+ questionText + ", coachingTip=" + coachingTip + ", isActive=" + isActive + ", tenantId=" + tenantId
				+ ", createDate=" + createDate + "]";
	}

}