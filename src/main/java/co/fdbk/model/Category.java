package co.fdbk.model;

import java.sql.Date;

public class Category extends Response {
	private int org_id;
	private int category_id;
	private int parent_category_id;
	private int user_id;
	private String category_name;
	private int is_active;
	private Date create_date;
	private String parent_category_name;
	private int is_deleted;

	public String getParent_category_name() {
		return parent_category_name;
	}

	public void setParent_category_name(String parent_category_name) {
		this.parent_category_name = parent_category_name;
	}

	public int getOrg_id() {
		return org_id;
	}

	public void setOrg_id(int org_id) {
		this.org_id = org_id;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getParent_category_id() {
		return parent_category_id;
	}

	public void setParent_category_id(int parent_category_id) {
		this.parent_category_id = parent_category_id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public int getIs_active() {
		return is_active;
	}

	public void setIs_active(int is_active) {
		this.is_active = is_active;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public int getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(int is_deleted) {
		this.is_deleted = is_deleted;
	}

	@Override
	public String toString() {
		return "Category [org_id=" + org_id + ", category_id=" + category_id + ", parent_category_id="
				+ parent_category_id + ", user_id=" + user_id + ", category_name=" + category_name + ", is_active="
				+ is_active + ", create_date=" + create_date + ", parent_category_name=" + parent_category_name
				+ ", is_deleted=" + is_deleted + "]";
	}

}
