package co.fdbk.model;

public class OtpVerify extends Response {
	private String userOtpId;
	private String verificationCode;
	
	public String getUserOtpId() {
		return userOtpId;
	}
	public void setUserOtpId(String userOtpId) {
		this.userOtpId = userOtpId;
	}
	public String getVerificationCode() {
		return verificationCode;
	}
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}
	@Override
	public String toString() {
		return "OtpVerify [userOtpId=" + userOtpId + ", verificationCode=" + verificationCode + "]";
	}
}