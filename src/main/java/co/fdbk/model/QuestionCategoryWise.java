package co.fdbk.model;

import java.sql.Date;

public class QuestionCategoryWise {
	private int questionId;
	private String questionText;
	private int categoryId;
	private String coachingTip;
	private Date createDate;
	private int isActive;

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getCoachingTip() {
		return coachingTip;
	}

	public void setCoachingTip(String coachingTip) {
		this.coachingTip = coachingTip;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "QuestionCategoryWise [questionId=" + questionId + ", questionText=" + questionText + ", categoryId="
				+ categoryId + ", coachingTip=" + coachingTip + ", createDate=" + createDate + ", isActive=" + isActive
				+ "]";
	}

}
