package co.fdbk.model;

public class Response {

	private int Ack=0;
	private String message="Not Found";
	

	public int getAck() {
		return Ack;
	}
	public void setAck(int ack) {
		Ack = ack;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		return "Response [Ack=" + Ack + ", message=" + message + "]";
	}
	
	
}