package co.fdbk.model;

public class Questions{
	private String categoryId;

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	@Override
	public String toString() {
		return "Questions [categoryId=" + categoryId + "]";
	}
	
}