package co.fdbk.model;

import java.util.Date;

public class UserLoginDetails extends Response {
	private Details details;

	public Details getDetails() {
		return details;
	}

	public void setDetails(Details details) {
		this.details = details;
	}

	public static class Details {

		private String userId;
		private String firstName;
		private String lastName;
		private String userName;
		private String emailAddress;
		private String countryId;
		private String countryCode;
		private String phone;
		private String linkedInId;
		private String linkedinEmail;
		private String linkedinUrl;
		private String jobTitle;
		private String location;
		private String deviceId;
		private String deviceType;
		private String identityPool;
		private String userPass;
		private String userImageId;
		private String originalFileUrl;
		private String resizeFileUrl;
		private Date create_date;
		private String password;
		private Date updatedDate;

		public String getUserImageId() {
			return userImageId;
		}

		public void setUserImageId(String userImageId) {
			this.userImageId = userImageId;
		}

		public String getOriginalFileUrl() {
			return originalFileUrl;
		}

		public void setOriginalFileUrl(String originalFileUrl) {
			this.originalFileUrl = originalFileUrl;
		}

		public String getResizeFileUrl() {
			return resizeFileUrl;
		}

		public void setResizeFileUrl(String resizeFileUrl) {
			this.resizeFileUrl = resizeFileUrl;
		}

		public Date getCreate_date() {
			return create_date;
		}

		public void setCreate_date(Date create_date) {
			this.create_date = create_date;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getEmailAddress() {
			return emailAddress;
		}

		public void setEmailAddress(String emailAddress) {
			this.emailAddress = emailAddress;
		}

		public String getCountryId() {
			return countryId;
		}

		public void setCountryId(String countryId) {
			this.countryId = countryId;
		}

		public String getCountryCode() {
			return countryCode;
		}

		public void setCountryCode(String countryCode) {
			this.countryCode = countryCode;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getLinkedinEmail() {
			return linkedinEmail;
		}

		public void setLinkedinEmail(String linkedinEmail) {
			this.linkedinEmail = linkedinEmail;
		}

		public String getLinkedinUrl() {
			return linkedinUrl;
		}

		public void setLinkedinUrl(String linkedinUrl) {
			this.linkedinUrl = linkedinUrl;
		}

		public String getJobTitle() {
			return jobTitle;
		}

		public void setJobTitle(String jobTitle) {
			this.jobTitle = jobTitle;
		}

		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}

		public String getDeviceId() {
			return deviceId;
		}

		public void setDeviceId(String deviceId) {
			this.deviceId = deviceId;
		}

		public String getDeviceType() {
			return deviceType;
		}

		public void setDeviceType(String deviceType) {
			this.deviceType = deviceType;
		}

		public String getIdentityPool() {
			return identityPool;
		}

		public void setIdentityPool(String identityPool) {
			this.identityPool = identityPool;
		}

		public String getLinkedInId() {
			return linkedInId;
		}

		public void setLinkedInId(String linkedInId) {
			this.linkedInId = linkedInId;
		}

		public String getUserPass() {
			return userPass;
		}

		public void setUserPass(String userPass) {
			this.userPass = userPass;
		}

		public Date getUpdatedDate() {
			return updatedDate;
		}

		public void setUpdatedDate(Date updatedDate) {
			this.updatedDate = updatedDate;
		}

		@Override
		public String toString() {
			return "Details [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", userName="
					+ userName + ", emailAddress=" + emailAddress + ", countryId=" + countryId + ", countryCode="
					+ countryCode + ", phone=" + phone + ", linkedInId=" + linkedInId + ", linkedinEmail="
					+ linkedinEmail + ", linkedinUrl=" + linkedinUrl + ", jobTitle=" + jobTitle + ", location="
					+ location + ", deviceId=" + deviceId + ", deviceType=" + deviceType + ", identityPool="
					+ identityPool + ", userPass=" + userPass + ", userImageId=" + userImageId + ", originalFileUrl="
					+ originalFileUrl + ", resizeFileUrl=" + resizeFileUrl + ", create_date=" + create_date
					+ ", password=" + password + ", updatedDate=" + updatedDate + "]";
		}

	}

	@Override
	public String toString() {
		return "UserLoginDetails [details=" + details + "]";
	}
	
}
