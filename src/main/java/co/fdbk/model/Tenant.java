package co.fdbk.model;

import java.util.Date;

public class Tenant extends Response {

	private String tenantId;
	private String orgName;
	private String orgAddress;
	private String orgAddress2;
	private String orgCountry;
	private String isDeleted;
	private Date createdDate;

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgAddress() {
		return orgAddress;
	}

	public void setOrgAddress(String orgAddress) {
		this.orgAddress = orgAddress;
	}

	public String getOrgAddress2() {
		return orgAddress2;
	}

	public void setOrgAddress2(String orgAddress2) {
		this.orgAddress2 = orgAddress2;
	}

	public String getOrgCountry() {
		return orgCountry;
	}

	public void setOrgCountry(String orgCountry) {
		this.orgCountry = orgCountry;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "Tenant [tenantId=" + tenantId + ", orgName=" + orgName + ", orgAddress=" + orgAddress + ", orgAddress2="
				+ orgAddress2 + ", orgCountry=" + orgCountry + ", isDeleted=" + isDeleted + ", createdDate="
				+ createdDate + "]";
	}
}