package co.fdbk.model;

public class AddMentor {
	private String firstName;
	private String lastName;
	private String userName;
	private String emailAddress;
	private String countryId;
	private String phone;
	private String linkedInId;
	private String linkedinEmail;
	private String linkedinUrl;
	private String deviceType;
	private String deviceId;
	private String jobTitle;
	private String location;
	private String userPass;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLinkedInId() {
		return linkedInId;
	}

	public void setLinkedInId(String linkedInId) {
		this.linkedInId = linkedInId;
	}

	public String getLinkedinEmail() {
		return linkedinEmail;
	}

	public void setLinkedinEmail(String linkedinEmail) {
		this.linkedinEmail = linkedinEmail;
	}

	public String getLinkedinUrl() {
		return linkedinUrl;
	}

	public void setLinkedinUrl(String linkedinUrl) {
		this.linkedinUrl = linkedinUrl;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getUserPass() {
		return userPass;
	}

	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}

	@Override
	public String toString() {
		return "AddMentor [firstName=" + firstName + ", lastName=" + lastName + ", userName=" + userName
				+ ", emailAddress=" + emailAddress + ", countryId=" + countryId + ", phone=" + phone + ", linkedInId="
				+ linkedInId + ", linkedinEmail=" + linkedinEmail + ", linkedinUrl=" + linkedinUrl + ", deviceType="
				+ deviceType + ", deviceId=" + deviceId + ", jobTitle=" + jobTitle + ", location=" + location
				+ ", userPass=" + userPass + "]";
	}
}