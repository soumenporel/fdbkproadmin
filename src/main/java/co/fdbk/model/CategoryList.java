package co.fdbk.model;

import java.util.Date;
import java.util.List;

public class CategoryList extends Response {
	private List<Parentcategory> parentcategoryList;
	private String categoryName;
	private int categoryId;
	private int userId;
	public List<Usercategory> getUsercategoryList() {
		return usercategoryList;
	}

	public void setUsercategoryList(List<Usercategory> usercategoryList) {
		this.usercategoryList = usercategoryList;
	}


	private List<Usercategory> usercategoryList;
	
	public String getCategoryName() {
		return categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public int getCategoryId() {
		return categoryId;
	}


	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public List<Parentcategory> getParentcategoryList() {
		return parentcategoryList;
	}


	public void setParentcategoryList(List<Parentcategory> parentcategoryList) {
		this.parentcategoryList = parentcategoryList;
	}
	public static class Usercategory
	{
	
		private String categoryName;
		private int categoryID;
		private int status;
		
		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		public String getCategoryName() {
			return categoryName;
		}
		public void setCategoryName(String categoryName) {
			this.categoryName = categoryName;
		}
		public int getCategoryID() {
			return categoryID;
		}
		public void setCategoryID(int categoryID) {
			this.categoryID = categoryID;
		}
		
	}

	public static class Parentcategory
	{
	
		private String parentCategoryName;
		private int parentId;
		private List<Child> childList;
		
		public String getParentCategoryName() {
			return parentCategoryName;
		}
		public void setParentCategoryName(String parentCategoryName) {
			this.parentCategoryName = parentCategoryName;
		}
		public int getParentId() {
			return parentId;
		}
		public void setParentId(int parentId) {
			this.parentId = parentId;
		}
		public List<Child> getChildList() {
			return childList;
		}
		public void setChildList(List<Child> childList) {
			this.childList = childList;
		}
		@Override
		public String toString() {
			return "Parentcategory [parentCategoryName=" + parentCategoryName + ", parentId=" + parentId
					+ ", childList=" + childList + "]";
		}
		
		
	}


	public static class Child
	{
		private int orgId;
		private int categoryId;
		private int parentCategoryId;
		private int userId;
		private String categoryName;
		private int isActive;
		private Date createDate;
		public int getOrgId() {
			return orgId;
		}
		public void setOrgId(int orgId) {
			this.orgId = orgId;
		}
		public int getCategoryId() {
			return categoryId;
		}
		public void setCategoryId(int categoryId) {
			this.categoryId = categoryId;
		}
		public int getParentCategoryId() {
			return parentCategoryId;
		}
		public void setParentCategoryId(int parentCategoryId) {
			this.parentCategoryId = parentCategoryId;
		}
		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}
		public String getCategoryName() {
			return categoryName;
		}
		public void setCategoryName(String categoryName) {
			this.categoryName = categoryName;
		}
		public int getIsActive() {
			return isActive;
		}
		public void setIsActive(int isActive) {
			this.isActive = isActive;
		}
		public Date getCreateDate() {
			return createDate;
		}
		public void setCreateDate(Date createDate) {
			this.createDate = createDate;
		}
		@Override
		public String toString() {
			return "Child [orgId=" + orgId + ", categoryId=" + categoryId + ", parentCategoryId=" + parentCategoryId
					+ ", userId=" + userId + ", categoryName=" + categoryName + ", isActive=" + isActive
					+ ", createDate=" + createDate + "]";
		}
	}

	@Override
	public String toString() {
		return "CategoryList [parentcategoryList=" + parentcategoryList + ", categoryName=" + categoryName
				+ ", categoryId=" + categoryId + ", userId=" + userId + ", usercategoryList=" + usercategoryList + "]";
	}

}