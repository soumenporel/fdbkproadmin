package co.fdbk.model;

public class QuestionDetailsPayload {
	private int questionId;

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	@Override
	public String toString() {
		return "QuestionDetailsPayload [questionId=" + questionId + "]";
	}
}
