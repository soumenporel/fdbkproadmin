package co.fdbk.model;

public class ChangePassword extends Response{
	private String userId;
	private String newPass;
	private String emailAddress;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getNewPass() {
		return newPass;
	}
	public void setNewPass(String newPass) {
		this.newPass = newPass;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	@Override
	public String toString() {
		return "ChangePassword [userId=" + userId + ", newPass=" + newPass + ","
				+ " emailAddress=" + emailAddress + "]";
	}
	
	
}