package co.fdbk.model;

import java.util.Date;

public class CMS extends Response {
	private String pageId;
	private String versionId;
	private String pageTitle;
	private String pageContent;
	private String metaKeyword;
	private String metaDescription;
	private String slug;
	private Date createDate;
	private String isActive;
	private String isDeleted;

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getPageContent() {
		return pageContent;
	}

	public void setPageContent(String pageContent) {
		this.pageContent = pageContent;
	}

	public String getMetaKeyword() {
		return metaKeyword;
	}

	public void setMetaKeyword(String metaKeyword) {
		this.metaKeyword = metaKeyword;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return "CMS [pageId=" + pageId + ", versionId=" + versionId + ", pageTitle=" + pageTitle + ", pageContent="
				+ pageContent + ", metaKeyword=" + metaKeyword + ", metaDescription=" + metaDescription + ", slug="
				+ slug + ", createDate=" + createDate + ", isActive=" + isActive + ", isDeleted=" + isDeleted + "]";
	}
}