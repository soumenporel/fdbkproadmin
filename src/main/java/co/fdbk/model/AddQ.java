package co.fdbk.model;

public class AddQ {
	
	private String questionText;
	private String coachingTip;

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public String getCoachingTip() {
		return coachingTip;
	}

	public void setCoachingTip(String coachingTip) {
		this.coachingTip = coachingTip;
	}

	@Override
	public String toString() {
		return "AddQ [questionText=" + questionText + ", coachingTip=" + coachingTip + "]";
	}
}