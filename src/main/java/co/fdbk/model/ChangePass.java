package co.fdbk.model;

public class ChangePass {
	private String userId;
	private String oldPass;
	private String newPass;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOldPass() {
		return oldPass;
	}

	public void setOldPass(String oldPass) {
		this.oldPass = oldPass;
	}

	public String getNewPass() {
		return newPass;
	}

	public void setNewPass(String newPass) {
		this.newPass = newPass;
	}

	@Override
	public String toString() {
		return "ChangePass [userId=" + userId + ", oldPass=" + oldPass + ", newPass=" + newPass + "]";
	}
}
