package co.fdbk.model;

import java.util.List;

public class QuestionPayload {
	private List<String> questionText;
	private List<String> coachingTip;

	public List<String> getQuestionText() {
		return questionText;
	}

	public void setQuestionText(List<String> questionText) {
		this.questionText = questionText;
	}

	public List<String> getCoachingTip() {
		return coachingTip;
	}

	public void setCoachingTip(List<String> coachingTip) {
		this.coachingTip = coachingTip;
	}

	@Override
	public String toString() {
		return "QuestionPayload [questionText=" + questionText + ", coachingTip=" + coachingTip + "]";
	}
}
